<?php

namespace LiqpayMagento\LiqPay\Setup;

use Magento\Config\Model\Config\Factory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Encryption\EncryptorInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var Factory
     */
    private $_configFactory;

    /**
     * @var EncryptorInterface
     */
    private $_encryptor;

    public function __construct(
        Factory $configFactory,
        EncryptorInterface $encryptor
    ) {
        $this->_configFactory = $configFactory;
        $this->_encryptor = $encryptor;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Exception
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $configData = [
            'section' => "payment",
            'website' => null,
            'store'   => null,
            'groups'  => [
                'liqpaymagento_liqpay' => [
                    'fields' => [
                        'active' => [
                            'value' => "1"
                        ],
                        'sandbox' => [
                            'value' => "1"
                        ],
                        'sandbox_order_surfix' => [
                            'value' => "test"
                        ],
                        'title' => [
                            'value' => "LiqPay"
                        ],
                        'public_key' => [
                            'value' => "sandbox_i89148165041"
                        ],
                        'private_key' => [
                            'value' => $this->_encryptor->encrypt("sandbox_0kqYRXMNGb7tMFzQNOG3Z68D6ka1qYAfLvYGDeP1")
                        ],
                        'allowspecific' => [
                            'value' => "0"
                        ],
                        'specificcountry' => [
                            'value' => ""
                        ],
                        'description' => [
                            'value' => "t"
                        ]
                    ]
                ]
            ]
        ];

        /** @var \Magento\Config\Model\Config $configModel */
        $configModel = $this->_configFactory->create(['data' => $configData]);
        $configModel->save();
    }
}
